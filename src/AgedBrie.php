<?php


namespace GildedRose;


class AgedBrie implements ItemInterface
{
    /**
     * @param Item $item
     */
    public static function updateQuality(Item $item)
    {
        $item->quality = min(++$item->quality, 50);

        if ($item->sell_in < 0) {
            $item->quality = min(++$item->quality, 50);
        }
    }
}