<?php

declare(strict_types=1);

namespace GildedRose;

final class GildedRose
{
    /**
     * @var Item[]
     */
    private $items;

    public function __construct(array $items)
    {
        $this->items = $items;
    }

    public static function of($name, $quality, $sellIn)
    {
        return new static($name, $quality, $sellIn);
    }

    public function updateQuality()
    {
        foreach ($this->items as $item) {
            $item->sell_in--;
            if ($item->name === 'Aged Brie') {

                AgedBrie::updateQuality($item);

            } elseif ($item->name === 'Backstage passes to a TAFKAL80ETC concert') {

                BackstagePasses::updateQuality($item);

            } elseif ($item->name === 'Sulfuras, Hand of Ragnaros') {

                $item->sell_in = $item->sell_in + 1;
                LegendaryItem::updateQuality($item);

            } else {

                StandardItem::updateQuality($item);

            }
        }
    }
}
