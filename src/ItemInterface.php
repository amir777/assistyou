<?php

namespace GildedRose;

interface ItemInterface
{
    public static function updateQuality(Item $item);
}