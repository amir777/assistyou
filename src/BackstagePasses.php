<?php


namespace GildedRose;


class BackstagePasses implements ItemInterface
{
    const TEN_DAYS = 10;
    const FIVE_DAYS = 5;

    public static function updateQuality(Item $item)
    {
        if ($item->quality < 50) {
            $item->quality++;
            if ($item->sell_in < self::TEN_DAYS) {
                $item->quality = min(++$item->quality, 50);
            }
            if ($item->sell_in < self::FIVE_DAYS) {
                $item->quality = min(++$item->quality, 50);
            }
        }

        if ($item->sell_in < 0) {
            $item->quality = 0;
        }
    }
}