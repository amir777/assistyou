<?php


namespace GildedRose;


class StandardItem implements ItemInterface
{
    public static function updateQuality(Item $item)
    {
        $item->quality = max(0, --$item->quality);

        if ($item->sell_in < 0) {
            $item->quality = max(0, --$item->quality);
        }
    }
}