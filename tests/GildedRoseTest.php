<?php

declare(strict_types=1);

namespace Tests;

use ArrayObject;
use GildedRose\GildedRose;
use GildedRose\GildedRoseGolden;
use GildedRose\Item;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function test_master(): void
    {
         $items = [
             new Item('+5 Dexterity Vest', 10, 20),
             new Item('Aged Brie', 2, 0),
             new Item('Elixir of the Mongoose', 5, 7),
             new Item('Sulfuras, Hand of Ragnaros', 0, 80),
             new Item('Sulfuras, Hand of Ragnaros', -1, 80),
             new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
             new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
             new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
         ];

         $itemsForGoldenMaster = [
             new Item('+5 Dexterity Vest', 10, 20),
             new Item('Aged Brie', 2, 0),
             new Item('Elixir of the Mongoose', 5, 7),
             new Item('Sulfuras, Hand of Ragnaros', 0, 80),
             new Item('Sulfuras, Hand of Ragnaros', -1, 80),
             new Item('Backstage passes to a TAFKAL80ETC concert', 15, 20),
             new Item('Backstage passes to a TAFKAL80ETC concert', 10, 49),
             new Item('Backstage passes to a TAFKAL80ETC concert', 5, 49),
         ];
        
        $gildedRose = new GildedRose($items);
        $goldenMaster = new GildedRoseGolden($itemsForGoldenMaster);

        $numOfItems = sizeof($items);
        
        for ($day=0; $day < 10; $day++) {
            $gildedRose->updateQuality();
            $goldenMaster->updateQuality();
            for ($i=0; $i < $numOfItems; $i++) {
                $this->assertEquals($items[$i], $itemsForGoldenMaster[$i]);
            }
        }
    }
}